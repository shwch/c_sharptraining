﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankingApplication
{
    public partial class AllCustomers : Form
    {
        public AllCustomers()
        {
            InitializeComponent();
            GetGridData();
        }

        private void GetGridData()
        {
            dGVAllCustomers.AutoGenerateColumns = false;

            banking_dbEntities5 bd = new banking_dbEntities5();
            var item = bd.UserAccounts.ToList();
          
            dGVAllCustomers.DataSource = item;
        }

        private void AllCustomers_Load(object sender, EventArgs e)
        {

        }

        private void dGVAllCustomers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
