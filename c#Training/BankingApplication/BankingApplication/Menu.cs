﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankingApplication
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void newAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void newAccountToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            NewAccount newacc = new NewAccount();
            newacc.MdiParent = this;
            newacc.Show();
        }

        private void updateSearchAccountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UpdateSearch updatesearch = new UpdateSearch();
            updatesearch.MdiParent = this;
            updatesearch.Show();
        }

        private void allCustomersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AllCustomers allcustomers = new AllCustomers();
            allcustomers.MdiParent = this;
            allcustomers.Show();
        }

        private void depositeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Deposite deposite = new Deposite();
            //deposite.MdiParent = this;
            //deposite.Show();
        }

        private void withdrawToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Withdraw withdraw = new Withdraw();
            withdraw.MdiParent = this;
            withdraw.Show();
        }

        private void transferToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Transfer transfer =new Transfer();
            //transfer.MdiParent = this;
            //transfer.Show();
        }

        private void fixDepositeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FixDeposite fixDeposite = new FixDeposite();
            fixDeposite.MdiParent = this;
            fixDeposite.Show();
        }

        private void balanceSheetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BalanceSheet balanceSheet =new BalanceSheet();
            balanceSheet.MdiParent = this;
            balanceSheet.Show();
        }

        private void viewFDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ViewFD viewFD = new ViewFD();
            viewFD.MdiParent = this;
            viewFD.Show();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        { 
            ChangePassword changepassword = new ChangePassword();
            changepassword.MdiParent = this;
            changepassword.Show();

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
