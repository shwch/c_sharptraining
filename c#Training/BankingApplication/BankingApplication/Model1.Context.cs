﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BankingApplication
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class banking_dbEntities5 : DbContext
    {
        public banking_dbEntities5()
            : base("name=banking_dbEntities5")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Admin_Table> Admin_Table { get; set; }
        public virtual DbSet<debit> debits { get; set; }
        public virtual DbSet<Deposite> Deposites { get; set; }
        public virtual DbSet<employee> employees { get; set; }
        public virtual DbSet<FD> FDs { get; set; }
        public virtual DbSet<Transfer> Transfers { get; set; }
        public virtual DbSet<UserAccount> UserAccounts { get; set; }
        public virtual DbSet<userTable> userTables { get; set; }
    }
}
