﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankingApplication
{
    public partial class NewAccount : Form
    {

        private string gender = string.Empty;
        private string maritalStatus = String.Empty;
        private decimal no;
        private banking_dbEntities5 BSE;
        
        private MemoryStream ms;
        
        public NewAccount()
        {
            InitializeComponent();
            LoadDate();
            LoadAccount();
            LoadState();

        }

        private void LoadState()
        {
            cmbxState.Items.Add("Gujarat");
        }

        private void LoadAccount()
        {
            BSE = new banking_dbEntities5();
            var item = BSE.UserAccounts.ToArray();
            no = item.LastOrDefault().Account_No + 1;
            txtAccountNo.Text = Convert.ToString(no);
    
        }

        private void LoadDate()
        {
            lblDateLabel.Text = DateTime.Now.ToShortDateString();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void NewAccount_Load(object sender, EventArgs e)
        {

        }

        private void rbtnFemale_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (rbtnMale.Checked)
            {
                gender = "Male";
            }
            else if (rbtnFemale.Checked)
            {
                gender = "Female";
            }
            else
            {
                gender = "Others";
            }

            if (rbtnMarried.Checked)
            {
                maritalStatus = "Married";
            }
           
            else
            {
                maritalStatus = "Unmarried";
            }



            BSE= new banking_dbEntities5();
            UserAccount acc = new UserAccount();
            acc.Account_No = Convert.ToDecimal(txtAccountNo.Text);
            acc.Name = txtName.Text;
            acc.DOB = dtpDOB.Value.ToString();
            acc.PhoneNumber = txtPhoneNo.Text;
            acc.Address = txtAddress.Text;
            acc.District = txtDist.Text;
            acc.State = cmbxState.SelectedItem.ToString();
            acc.Gender = gender;
            acc.MaritalStatus = maritalStatus;
            acc.Mother_Name = txtMotherName.Text;
            acc.Father_Name = txtFatherName.Text;
            acc.Balance = Convert.ToDecimal(txtBalance.Text);
            acc.Date = lblDateLabel.Text;
            acc.Picture = ms.ToArray();
            BSE.UserAccounts.Add(acc);
            BSE.SaveChanges();
            MessageBox.Show("File Saved");

        }

        private void btnUploadPhoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog opendlg = new OpenFileDialog();
            if (opendlg.ShowDialog() == DialogResult.OK)
            {
                Image img = Image.FromFile(opendlg.FileName);
                pictureBox1.Image = img;
                ms = new MemoryStream();
                img.Save(ms, img.RawFormat);
            }
        }
    }
}
