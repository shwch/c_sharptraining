﻿namespace BankingApplication
{
    partial class UpdateSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUpdateForm = new System.Windows.Forms.Label();
            this.lblAccountNo = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblDOB = new System.Windows.Forms.Label();
            this.lblMotherName = new System.Windows.Forms.Label();
            this.lblFatherName = new System.Windows.Forms.Label();
            this.lblPhoneNumber = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblDist = new System.Windows.Forms.Label();
            this.lblState = new System.Windows.Forms.Label();
            this.txtAccountNo = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtMotherName = new System.Windows.Forms.TextBox();
            this.txtFatherName = new System.Windows.Forms.TextBox();
            this.txtPhoneNo = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtDist = new System.Windows.Forms.TextBox();
            this.dTPDOB = new System.Windows.Forms.DateTimePicker();
            this.btnDetails = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lblUploadFile = new System.Windows.Forms.Button();
            this.pbxUploadPic = new System.Windows.Forms.PictureBox();
            this.grpGender = new System.Windows.Forms.GroupBox();
            this.rbtnOthers = new System.Windows.Forms.RadioButton();
            this.rbtnFemale = new System.Windows.Forms.RadioButton();
            this.rbtnMale = new System.Windows.Forms.RadioButton();
            this.grpMaritalStatus = new System.Windows.Forms.GroupBox();
            this.rbtnUnmarried = new System.Windows.Forms.RadioButton();
            this.rbtnMarried = new System.Windows.Forms.RadioButton();
            this.dGVUpdateForm = new System.Windows.Forms.DataGridView();
            this.txtState = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxUploadPic)).BeginInit();
            this.grpGender.SuspendLayout();
            this.grpMaritalStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGVUpdateForm)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUpdateForm
            // 
            this.lblUpdateForm.AutoSize = true;
            this.lblUpdateForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpdateForm.Location = new System.Drawing.Point(202, 33);
            this.lblUpdateForm.Name = "lblUpdateForm";
            this.lblUpdateForm.Size = new System.Drawing.Size(131, 24);
            this.lblUpdateForm.TabIndex = 0;
            this.lblUpdateForm.Text = "Update Form";
            // 
            // lblAccountNo
            // 
            this.lblAccountNo.AutoSize = true;
            this.lblAccountNo.Location = new System.Drawing.Point(47, 133);
            this.lblAccountNo.Name = "lblAccountNo";
            this.lblAccountNo.Size = new System.Drawing.Size(81, 17);
            this.lblAccountNo.TabIndex = 1;
            this.lblAccountNo.Text = "Account No";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(47, 185);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(45, 17);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "Name";
            // 
            // lblDOB
            // 
            this.lblDOB.AutoSize = true;
            this.lblDOB.Location = new System.Drawing.Point(47, 245);
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Size = new System.Drawing.Size(90, 17);
            this.lblDOB.TabIndex = 3;
            this.lblDOB.Text = "Date Of Birth";
            // 
            // lblMotherName
            // 
            this.lblMotherName.AutoSize = true;
            this.lblMotherName.Location = new System.Drawing.Point(47, 303);
            this.lblMotherName.Name = "lblMotherName";
            this.lblMotherName.Size = new System.Drawing.Size(103, 17);
            this.lblMotherName.TabIndex = 4;
            this.lblMotherName.Text = "Mother\'s Name";
            // 
            // lblFatherName
            // 
            this.lblFatherName.AutoSize = true;
            this.lblFatherName.Location = new System.Drawing.Point(47, 356);
            this.lblFatherName.Name = "lblFatherName";
            this.lblFatherName.Size = new System.Drawing.Size(100, 17);
            this.lblFatherName.TabIndex = 5;
            this.lblFatherName.Text = "Father\'s Name";
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.AutoSize = true;
            this.lblPhoneNumber.Location = new System.Drawing.Point(47, 420);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(103, 17);
            this.lblPhoneNumber.TabIndex = 6;
            this.lblPhoneNumber.Text = "Phone Number";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(47, 485);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(60, 17);
            this.lblAddress.TabIndex = 7;
            this.lblAddress.Text = "Address";
            // 
            // lblDist
            // 
            this.lblDist.AutoSize = true;
            this.lblDist.Location = new System.Drawing.Point(47, 545);
            this.lblDist.Name = "lblDist";
            this.lblDist.Size = new System.Drawing.Size(51, 17);
            this.lblDist.TabIndex = 8;
            this.lblDist.Text = "District";
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(47, 601);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(41, 17);
            this.lblState.TabIndex = 9;
            this.lblState.Text = "State";
            this.lblState.Click += new System.EventHandler(this.label9_Click);
            // 
            // txtAccountNo
            // 
            this.txtAccountNo.Location = new System.Drawing.Point(175, 127);
            this.txtAccountNo.Name = "txtAccountNo";
            this.txtAccountNo.Size = new System.Drawing.Size(200, 22);
            this.txtAccountNo.TabIndex = 10;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(175, 180);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(200, 22);
            this.txtName.TabIndex = 11;
            // 
            // txtMotherName
            // 
            this.txtMotherName.Location = new System.Drawing.Point(175, 298);
            this.txtMotherName.Name = "txtMotherName";
            this.txtMotherName.Size = new System.Drawing.Size(200, 22);
            this.txtMotherName.TabIndex = 12;
            // 
            // txtFatherName
            // 
            this.txtFatherName.Location = new System.Drawing.Point(175, 351);
            this.txtFatherName.Name = "txtFatherName";
            this.txtFatherName.Size = new System.Drawing.Size(200, 22);
            this.txtFatherName.TabIndex = 13;
            // 
            // txtPhoneNo
            // 
            this.txtPhoneNo.Location = new System.Drawing.Point(175, 415);
            this.txtPhoneNo.Name = "txtPhoneNo";
            this.txtPhoneNo.Size = new System.Drawing.Size(200, 22);
            this.txtPhoneNo.TabIndex = 14;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(175, 459);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(200, 43);
            this.txtAddress.TabIndex = 15;
            // 
            // txtDist
            // 
            this.txtDist.Location = new System.Drawing.Point(175, 540);
            this.txtDist.Name = "txtDist";
            this.txtDist.Size = new System.Drawing.Size(200, 22);
            this.txtDist.TabIndex = 16;
            // 
            // dTPDOB
            // 
            this.dTPDOB.Location = new System.Drawing.Point(175, 239);
            this.dTPDOB.Name = "dTPDOB";
            this.dTPDOB.Size = new System.Drawing.Size(200, 22);
            this.dTPDOB.TabIndex = 17;
            // 
            // btnDetails
            // 
            this.btnDetails.Location = new System.Drawing.Point(405, 121);
            this.btnDetails.Name = "btnDetails";
            this.btnDetails.Size = new System.Drawing.Size(120, 28);
            this.btnDetails.TabIndex = 19;
            this.btnDetails.Text = "Details";
            this.btnDetails.UseVisualStyleBackColor = true;
            this.btnDetails.Click += new System.EventHandler(this.btnDetails_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(405, 174);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(120, 28);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(353, 642);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(120, 28);
            this.btnDelete.TabIndex = 21;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(175, 642);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(120, 28);
            this.btnUpdate.TabIndex = 22;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lblUploadFile
            // 
            this.lblUploadFile.Location = new System.Drawing.Point(556, 234);
            this.lblUploadFile.Name = "lblUploadFile";
            this.lblUploadFile.Size = new System.Drawing.Size(120, 28);
            this.lblUploadFile.TabIndex = 25;
            this.lblUploadFile.Text = "Upload File";
            this.lblUploadFile.UseVisualStyleBackColor = true;
            this.lblUploadFile.Click += new System.EventHandler(this.lblUploadFile_Click);
            // 
            // pbxUploadPic
            // 
            this.pbxUploadPic.Location = new System.Drawing.Point(557, 33);
            this.pbxUploadPic.Name = "pbxUploadPic";
            this.pbxUploadPic.Size = new System.Drawing.Size(261, 185);
            this.pbxUploadPic.TabIndex = 26;
            this.pbxUploadPic.TabStop = false;
            this.pbxUploadPic.Click += new System.EventHandler(this.pbxUploadPic_Click);
            // 
            // grpGender
            // 
            this.grpGender.Controls.Add(this.rbtnOthers);
            this.grpGender.Controls.Add(this.rbtnFemale);
            this.grpGender.Controls.Add(this.rbtnMale);
            this.grpGender.Location = new System.Drawing.Point(556, 298);
            this.grpGender.Name = "grpGender";
            this.grpGender.Size = new System.Drawing.Size(335, 66);
            this.grpGender.TabIndex = 27;
            this.grpGender.TabStop = false;
            this.grpGender.Text = "Gender";
            // 
            // rbtnOthers
            // 
            this.rbtnOthers.AutoSize = true;
            this.rbtnOthers.Location = new System.Drawing.Point(190, 22);
            this.rbtnOthers.Name = "rbtnOthers";
            this.rbtnOthers.Size = new System.Drawing.Size(72, 21);
            this.rbtnOthers.TabIndex = 2;
            this.rbtnOthers.TabStop = true;
            this.rbtnOthers.Text = "Others";
            this.rbtnOthers.UseVisualStyleBackColor = true;
            // 
            // rbtnFemale
            // 
            this.rbtnFemale.AutoSize = true;
            this.rbtnFemale.Location = new System.Drawing.Point(92, 22);
            this.rbtnFemale.Name = "rbtnFemale";
            this.rbtnFemale.Size = new System.Drawing.Size(75, 21);
            this.rbtnFemale.TabIndex = 1;
            this.rbtnFemale.TabStop = true;
            this.rbtnFemale.Text = "Female";
            this.rbtnFemale.UseVisualStyleBackColor = true;
            // 
            // rbtnMale
            // 
            this.rbtnMale.AutoSize = true;
            this.rbtnMale.Location = new System.Drawing.Point(7, 22);
            this.rbtnMale.Name = "rbtnMale";
            this.rbtnMale.Size = new System.Drawing.Size(59, 21);
            this.rbtnMale.TabIndex = 0;
            this.rbtnMale.TabStop = true;
            this.rbtnMale.Text = "Male";
            this.rbtnMale.UseVisualStyleBackColor = true;
            // 
            // grpMaritalStatus
            // 
            this.grpMaritalStatus.Controls.Add(this.rbtnUnmarried);
            this.grpMaritalStatus.Controls.Add(this.rbtnMarried);
            this.grpMaritalStatus.Location = new System.Drawing.Point(556, 397);
            this.grpMaritalStatus.Name = "grpMaritalStatus";
            this.grpMaritalStatus.Size = new System.Drawing.Size(335, 66);
            this.grpMaritalStatus.TabIndex = 14;
            this.grpMaritalStatus.TabStop = false;
            this.grpMaritalStatus.Text = "Marital Status";
            // 
            // rbtnUnmarried
            // 
            this.rbtnUnmarried.AutoSize = true;
            this.rbtnUnmarried.Location = new System.Drawing.Point(129, 23);
            this.rbtnUnmarried.Name = "rbtnUnmarried";
            this.rbtnUnmarried.Size = new System.Drawing.Size(95, 21);
            this.rbtnUnmarried.TabIndex = 1;
            this.rbtnUnmarried.TabStop = true;
            this.rbtnUnmarried.Text = "Unmarried";
            this.rbtnUnmarried.UseVisualStyleBackColor = true;
            // 
            // rbtnMarried
            // 
            this.rbtnMarried.AutoSize = true;
            this.rbtnMarried.Location = new System.Drawing.Point(7, 22);
            this.rbtnMarried.Name = "rbtnMarried";
            this.rbtnMarried.Size = new System.Drawing.Size(77, 21);
            this.rbtnMarried.TabIndex = 0;
            this.rbtnMarried.TabStop = true;
            this.rbtnMarried.Text = "Married";
            this.rbtnMarried.UseVisualStyleBackColor = true;
            // 
            // dGVUpdateForm
            // 
            this.dGVUpdateForm.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVUpdateForm.Location = new System.Drawing.Point(885, 33);
            this.dGVUpdateForm.Name = "dGVUpdateForm";
            this.dGVUpdateForm.RowTemplate.Height = 24;
            this.dGVUpdateForm.Size = new System.Drawing.Size(474, 637);
            this.dGVUpdateForm.TabIndex = 28;
            this.dGVUpdateForm.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dGVUpdateForm_CellClick);
            // 
            // txtState
            // 
            this.txtState.Location = new System.Drawing.Point(175, 596);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(200, 22);
            this.txtState.TabIndex = 29;
            // 
            // UpdateSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1371, 709);
            this.Controls.Add(this.txtState);
            this.Controls.Add(this.dGVUpdateForm);
            this.Controls.Add(this.grpMaritalStatus);
            this.Controls.Add(this.grpGender);
            this.Controls.Add(this.pbxUploadPic);
            this.Controls.Add(this.lblUploadFile);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnDetails);
            this.Controls.Add(this.dTPDOB);
            this.Controls.Add(this.txtDist);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.txtPhoneNo);
            this.Controls.Add(this.txtFatherName);
            this.Controls.Add(this.txtMotherName);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtAccountNo);
            this.Controls.Add(this.lblState);
            this.Controls.Add(this.lblDist);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.lblPhoneNumber);
            this.Controls.Add(this.lblFatherName);
            this.Controls.Add(this.lblMotherName);
            this.Controls.Add(this.lblDOB);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblAccountNo);
            this.Controls.Add(this.lblUpdateForm);
            this.Name = "UpdateSearch";
            this.Text = "UpdateSearch";
            this.Load += new System.EventHandler(this.UpdateSearch_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxUploadPic)).EndInit();
            this.grpGender.ResumeLayout(false);
            this.grpGender.PerformLayout();
            this.grpMaritalStatus.ResumeLayout(false);
            this.grpMaritalStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGVUpdateForm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUpdateForm;
        private System.Windows.Forms.Label lblAccountNo;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblDOB;
        private System.Windows.Forms.Label lblMotherName;
        private System.Windows.Forms.Label lblFatherName;
        private System.Windows.Forms.Label lblPhoneNumber;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblDist;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.TextBox txtAccountNo;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtMotherName;
        private System.Windows.Forms.TextBox txtFatherName;
        private System.Windows.Forms.TextBox txtPhoneNo;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtDist;
        private System.Windows.Forms.DateTimePicker dTPDOB;
        private System.Windows.Forms.Button btnDetails;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button lblUploadFile;
        private System.Windows.Forms.PictureBox pbxUploadPic;
        private System.Windows.Forms.GroupBox grpGender;
        private System.Windows.Forms.RadioButton rbtnOthers;
        private System.Windows.Forms.RadioButton rbtnFemale;
        private System.Windows.Forms.RadioButton rbtnMale;
        private System.Windows.Forms.GroupBox grpMaritalStatus;
        private System.Windows.Forms.RadioButton rbtnUnmarried;
        private System.Windows.Forms.RadioButton rbtnMarried;
        private System.Windows.Forms.DataGridView dGVUpdateForm;
        private System.Windows.Forms.TextBox txtState;
    }
}