﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankingApplication
{
    public partial class UpdateSearch : Form
    {

        private  banking_dbEntities5 dbe;
        private MemoryStream ms;
        private BindingList<UserAccount> bi;
         
        public UpdateSearch()
        {
            InitializeComponent();
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void pbxUploadPic_Click(object sender, EventArgs e)
        {

        }

        private void UpdateSearch_Load(object sender, EventArgs e)
        {

        }

        private void btnDetails_Click(object sender, EventArgs e)
        {
            bi= new BindingList<UserAccount>();
            dbe= new banking_dbEntities5();
            decimal accno = Convert.ToDecimal(txtAccountNo.Text);
            var item = dbe.UserAccounts.Where(a => a.Account_No == accno);

            foreach (var item1 in item)
            {
                bi.Add(item1);

            }
            dGVUpdateForm.DataSource = bi;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            bi = new BindingList<UserAccount>();
            dbe = new banking_dbEntities5();
            var item = dbe.UserAccounts.Where(a => a.Name == txtName.Text);
            foreach (var item1 in item)
            {
                bi.Add(item1);

            }
            dGVUpdateForm.DataSource = bi;
        }

        private void dGVUpdateForm_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dbe = new banking_dbEntities5();
            decimal accno = Convert.ToDecimal(dGVUpdateForm.Rows[e.RowIndex].Cells[0].Value);
            var item = dbe.UserAccounts.Where(a => a.Account_No == accno).FirstOrDefault();
            txtAccountNo.Text = item.Account_No.ToString();
            txtName.Text = item.Name;
            txtMotherName.Text = item.Mother_Name;
            txtFatherName.Text = item.Father_Name;
            txtPhoneNo.Text = item.PhoneNumber;
            txtAddress.Text = item.Address;
            byte[] img = item.Picture;
            MemoryStream ms = new MemoryStream(img);
            pbxUploadPic.Image = Image.FromStream(ms);
            txtDist.Text = item.District;
            txtState.Text = item.State;

            if (item.Gender == "Male")
            {
                rbtnMale.Checked = true;
            }
            else if (item.Gender == "FeMale")
            {
                rbtnFemale.Checked = true;
            }
            else
            {
                rbtnOthers.Checked = true;
            }

            if (item.MaritalStatus == "Married")
            {
                rbtnMarried.Checked = true;
            }
            else 
            {
                rbtnUnmarried.Checked = true;
            }
        }

        private void lblUploadFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog opendlg = new OpenFileDialog();
            if (opendlg.ShowDialog() == DialogResult.OK)
            {
                Image img = Image.FromFile(opendlg.FileName);
                pbxUploadPic.Image = img;
                ms= new MemoryStream();
                img.Save(ms,img.RawFormat);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            bi.RemoveAt(dGVUpdateForm.SelectedRows[0].Index);
            dbe= new banking_dbEntities5();
            decimal a = Convert.ToDecimal(txtAccountNo.Text);
            UserAccount acc = dbe.UserAccounts.First(s => s.Account_No.Equals(a));
            dbe.UserAccounts.Remove(acc);
            dbe.SaveChanges();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            OpenFileDialog opendlg = new OpenFileDialog();

            if (opendlg.ShowDialog() == DialogResult.OK)
            {
                Image image = Image.FromFile(opendlg.FileName);
                pbxUploadPic.Image = image;
                image.Save(ms,image.RawFormat);

            }
        }
    }
}
