﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ConstantCheck
{
    class Program
    {
        static void Main(string[] args)
        {
           // int x = 10;
            //const int y = x/2;
            //const int a = 10 / 2;
            //a++;

            // string str = "Hi";
            //// str[1] = 'a';  string cannot be assigned value directly

            // Console.WriteLine(str);

            string s = "xyz";
            //x= int.Parse(s); // format exception

            // x = Convert.ToInt32(s); // format exception. since text cannot be given in string.

            int x = 0;
            int.TryParse(s, out x); // since tryparse method doesnt have any return type we need to initial x =0 and then stored value in x.
            
            Console.WriteLine(x);
            Console.ReadLine();

        }
    }
}
