﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringApplication
{
    class StringProgram1
    {
        static void Main(string[] args)
        {
            //string abc = "abc";
            //string xyz = "abc";

            //if (abc == xyz) // checks objects reference
            //{
            //    Console.WriteLine("same");
            //}
            //else
            //{
            //    Console.WriteLine(" not same");

            //}

            //if (abc.Equals(xyz)) // checks value of object
            //{
            //    Console.WriteLine("same");
            //}
            //else
            //{
            //    Console.WriteLine(" not same");

           // }

            // to make other object for the same value, using two different objects 
            string s1 = "abc";
           // string s2 = new string("abc"); // gives error so need to make char[] like below
            string s2 = new string(new char[] {'a','b','c'});


            //if (s1 == s2) // checks objects reference
            //{
            //    Console.WriteLine("same");
            //}
            //else
            //{
            //    Console.WriteLine(" not same");

            //}

            //if (s1.Equals(s2)) // checks value of object
            //{
            //    Console.WriteLine("same");
            //}
            //else
            //{
            //    Console.WriteLine(" not same");

            //}

            //StringBuilder sb = new StringBuilder();
            //sb.Append("hello");
            //sb.Append("Bye");

            string a1 = "abc";
            string a2 = "abc";

            Class1 c = new Class1();
            c.str = "abc";

            Console.WriteLine(a1==a2); //true bool value
            Console.WriteLine(a1.Equals(a2));
            Console.Read();

        }
    }
}
