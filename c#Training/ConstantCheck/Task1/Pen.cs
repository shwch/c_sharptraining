﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Pen
    {

        string color, brand;
        double price;
        int size, discountRate;
        bool isFountain;


        public Pen()
        {
            Console.WriteLine("Default Constructor called");
            this.color = "Red";
            this.price = 100;
            this.size = 12;
            this.discountRate = 20;
            this.isFountain = true;

        }
        public Pen(string colour, bool fountain)
        {
            Console.WriteLine("Parameterised Constructor called");
            this.color = colour;
            this.isFountain = fountain;
        }

        public Pen(Pen p)
        {
            Console.WriteLine("Copy Constructor called");
            this.color = p.color;
            this.brand = p.brand;
            this.price = p.price;
            this.size = p.size;
            this.discountRate = p.discountRate;
            this.isFountain = p.isFountain;
        }


        public void SetPenInfo(string colourOfPen, string brandOfPen, int priceOfPen, int sizeOfPen, int discountOnPen, bool isPenFountain)
        {
            this.color = colourOfPen;
            this.brand = brandOfPen;
            this.price = priceOfPen;
            this.size = sizeOfPen;
            this.discountRate = discountOnPen;
            this.isFountain = isPenFountain;
        }

        public void GetPenInfo()
        {
            Console.WriteLine("Colour of pen is :" + color + " " + "Brand of pen is :" + brand + " " + "Price of pen is :" + price + " " + "Size of pen is :" + size + " " + "Discount on pen is :" + discountRate + " " + "Is it a fountain pen? " + isFountain);

        }

        public void GetPrice()
        {
            Console.WriteLine("Cost Price of Pen is: " + price);
            double sellingPrice = GiveDiscount(discountRate);
            Console.WriteLine("Selling Price of Pen after discount is: " + sellingPrice);
        }

        public double GiveDiscount(int discountedRate)
        {
            double sellingPrice = price - ((price * discountedRate) / 100);
            return sellingPrice;
        }
    }
}
