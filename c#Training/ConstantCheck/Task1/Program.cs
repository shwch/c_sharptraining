﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            Pen p = new Pen();
            p.SetPenInfo("Blue", "Renolds", 100, 15, 20, false);
            p.GetPenInfo();
            p.GetPrice();


            Pen p1 = new Pen("Orange", true);
            p1.SetPenInfo("Red", "fountain", 200, 20, 30, true);
            p1.GetPenInfo();
            p1.GetPrice();

            Pen p3 = new Pen(p);
            p3.GetPenInfo();

            Console.ReadKey();
            
        }
    }
}
