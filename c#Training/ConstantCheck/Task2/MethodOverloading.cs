﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    internal class MethodOverloading
    {
        private int salary, bonus;
        private string name, lastname;
        private double houseAllowance, medicalAllowance, travelAllowance;
        private string employeeCategory;

        public MethodOverloading()
        {
            this.salary = 20000;
        }

        public void EmployeeDetail(string ename, string elastname, string eCategory)
        {
            int netSalary = salary + bonus;
            Console.WriteLine("Employee name:" + ename + "   Employee lastname:" + elastname + "  EmployeeCategory is:" + eCategory + "   Salary of Employee is :" + netSalary);
        }

        public void EmployeeDetail(string ename, string elastname, string eCategory, int esalary, int ebonus, double ehouseAllowance)
        {
            double netSalary = esalary + ebonus + ehouseAllowance;
            Console.WriteLine("Employee name:" + ename + "  Employee lastname:" + lastname + "    EmployeeCategory is:" + eCategory + "   Salary of Employee is :" + netSalary);
        }
        public void EmployeeDetail(string ename, string elastname, string eCategory, double houseAllowance, double medicalAllowance, double travelAllowance)
        {
            double netSalary = salary + bonus + houseAllowance + medicalAllowance + travelAllowance;
            Console.WriteLine("Employee name:" + ename + "  Employee lastname:" + elastname + "     EmployeeCategory is:" + eCategory + "   Salary of Employee is :" + netSalary);

        }



    }
}
