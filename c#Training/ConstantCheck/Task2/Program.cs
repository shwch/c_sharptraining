﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            MethodOverloading mo = new MethodOverloading();
            mo.EmployeeDetail("Shweta","Chitte","C");
            mo.EmployeeDetail("Pranita","Gore","B",70000,2000,1500);
            mo.EmployeeDetail("Kavita","Gupta","A",2000.0,325.50,3000.0);
            Console.WriteLine();
            Console.ReadKey();

        }
    }
}
