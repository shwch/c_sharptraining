﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Pen
    {
        static string brand;
        private string color;
        private double price;
        static int size , discountRate;
        static bool isFountain;
      
        static Pen()
        {
          
            brand = "Fountain";
            isFountain = true;
            size = 12;
            discountRate = 20;
            
        }

        public Pen(string colour, double priceOfPen)
        {
           
            color = colour;
            price = priceOfPen;

        }
        
        public string DisplayPen()
        {
            return "Pen Details are:" + "\r\n" + " Colour of pen is:" + this.color + "\r\n" + " Brand of pen is:"+ brand + "\r\n" + " Size of Pen is:"+ size + "\r\n" + " Price of pen is:" + price + "\r\n";
        }


        public double CalDiscount()
        {
            return price = price - ((price*discountRate)/100);
        }
        
    }
}
