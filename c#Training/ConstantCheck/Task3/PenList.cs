﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
   static class PenList
    {
        private static Pen[] penList;

        static PenList()
        {
            penList = new Pen[5];  
        }
        public static void SetPenDetails()
        {
                penList[0] = new Pen("Red",100);
                penList[1] =new Pen("Blue",130);
                penList[2] = new Pen("Green", 150);
                penList[3] = new Pen("Yellow", 80);
                penList[4] = new Pen("Pink",60);
            
        }
        public static Pen[] GetPenDetails()
        {
            return penList;
        }

    }
}
