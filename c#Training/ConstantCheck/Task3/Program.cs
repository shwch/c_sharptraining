﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            PenList.SetPenDetails();
            Pen[] penInfo = PenList.GetPenDetails();

            foreach (Pen p in penInfo)
            {
                Console.WriteLine(p.DisplayPen());
            }

            foreach (Pen p in penInfo)
            {
                Console.WriteLine("Details After discount are");
                Console.WriteLine(p.CalDiscount());
                Console.WriteLine(p.DisplayPen());
            }

            Console.ReadKey();
        }
    }
}
