﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteFile();
            ReadFile();
            Console.Read();
        }


        private static void WriteFile()
        {
            FileStream fs = null;
            StreamWriter sw = null;

            try
            {
           fs = new FileStream(@"F:\fileHandling\data.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite,
           FileShare.Read);
                sw = new StreamWriter(fs);
                sw.WriteLine("Hello World...");
                sw.WriteLine("Good Morning!!!");
                Console.WriteLine("Success");

                sw.Close();
                fs.Close();
            }
            catch (FileNotFoundException notfound)
            {
                Console.WriteLine(notfound.Message);
            }
            catch (IOException ioEx)
            {
                Console.WriteLine(ioEx.Message);
            }
            catch (UnauthorizedAccessException unauthEx)
            {
                Console.WriteLine(unauthEx.Message);
            }

            finally
            {
                sw.Close();
                fs.Close();

            }
        }


        private static void ReadFile()
        {
            FileStream fs = null;
            StreamReader sr = null;

            try
            {
                fs = new FileStream(@"F:\fileHandling\data.txt", FileMode.Open, FileAccess.Read,
                FileShare.ReadWrite);
                sr = new StreamReader(fs);

                string line = "";

                while ((line = sr.ReadLine() )!= null)
                {
                    Console.WriteLine(line);
                }

            }
            catch (FileNotFoundException notfound)
            {
                Console.WriteLine(notfound.Message);
            }
            catch (IOException ioEx)
            {
                Console.WriteLine(ioEx.Message);
            }
            catch (UnauthorizedAccessException unauthEx)
            {
                Console.WriteLine(unauthEx.Message);
            }

            finally
            {
                sr.Close();
                fs.Close();

            }
        }
    }
}
