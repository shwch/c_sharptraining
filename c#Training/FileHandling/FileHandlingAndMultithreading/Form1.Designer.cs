﻿namespace FileHandlingAndMultithreading
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnRead = new System.Windows.Forms.Button();
            this.txtReadFile = new System.Windows.Forms.TextBox();
            this.btnWrite = new System.Windows.Forms.Button();
            this.txtWriteData = new System.Windows.Forms.TextBox();
            this.btnBrowseRead = new System.Windows.Forms.Button();
            this.listboxRead = new System.Windows.Forms.ListBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(188, 132);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(216, 41);
            this.btnRead.TabIndex = 0;
            this.btnRead.Text = "Read Data";
            this.btnRead.UseVisualStyleBackColor = true;
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // txtReadFile
            // 
            this.txtReadFile.Location = new System.Drawing.Point(176, 52);
            this.txtReadFile.Multiline = true;
            this.txtReadFile.Name = "txtReadFile";
            this.txtReadFile.Size = new System.Drawing.Size(337, 40);
            this.txtReadFile.TabIndex = 1;
            this.txtReadFile.TextChanged += new System.EventHandler(this.txtReadFile_TextChanged);
            // 
            // btnWrite
            // 
            this.btnWrite.Location = new System.Drawing.Point(597, 52);
            this.btnWrite.Name = "btnWrite";
            this.btnWrite.Size = new System.Drawing.Size(108, 51);
            this.btnWrite.TabIndex = 3;
            this.btnWrite.Text = "Browse Write File";
            this.btnWrite.UseVisualStyleBackColor = true;
            this.btnWrite.Click += new System.EventHandler(this.btnWrite_Click);
            // 
            // txtWriteData
            // 
            this.txtWriteData.Location = new System.Drawing.Point(711, 52);
            this.txtWriteData.Multiline = true;
            this.txtWriteData.Name = "txtWriteData";
            this.txtWriteData.Size = new System.Drawing.Size(337, 40);
            this.txtWriteData.TabIndex = 4;
            this.txtWriteData.TextChanged += new System.EventHandler(this.txtWriteData_TextChanged);
            // 
            // btnBrowseRead
            // 
            this.btnBrowseRead.Location = new System.Drawing.Point(39, 51);
            this.btnBrowseRead.Name = "btnBrowseRead";
            this.btnBrowseRead.Size = new System.Drawing.Size(108, 41);
            this.btnBrowseRead.TabIndex = 5;
            this.btnBrowseRead.Text = "Browse Read File";
            this.btnBrowseRead.UseVisualStyleBackColor = true;
            this.btnBrowseRead.Click += new System.EventHandler(this.btnBrowseRead_Click);
            // 
            // listboxRead
            // 
            this.listboxRead.FormattingEnabled = true;
            this.listboxRead.ItemHeight = 16;
            this.listboxRead.Location = new System.Drawing.Point(39, 194);
            this.listboxRead.Name = "listboxRead";
            this.listboxRead.Size = new System.Drawing.Size(474, 436);
            this.listboxRead.TabIndex = 2;
            this.listboxRead.SelectedIndexChanged += new System.EventHandler(this.listboxRead_SelectedIndexChanged);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.CheckFileExists = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(597, 132);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 41);
            this.button1.TabIndex = 6;
            this.button1.Text = "Write Data";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1060, 760);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnBrowseRead);
            this.Controls.Add(this.txtWriteData);
            this.Controls.Add(this.btnWrite);
            this.Controls.Add(this.listboxRead);
            this.Controls.Add(this.txtReadFile);
            this.Controls.Add(this.btnRead);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnRead;
        private System.Windows.Forms.TextBox txtReadFile;
        private System.Windows.Forms.Button btnWrite;
        private System.Windows.Forms.TextBox txtWriteData;
        private System.Windows.Forms.Button btnBrowseRead;
        private System.Windows.Forms.ListBox listboxRead;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button button1;
    }
}

