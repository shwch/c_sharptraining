﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileHandlingAndMultithreading
{
    public partial class Form1 : Form
    {

        Thread readThread;
        Thread writeThread;
        public Form1()
        {
            InitializeComponent();
        }

        private void txtReadFile_TextChanged(object sender, EventArgs e)
        {
            txtReadFile.Enabled = false;
        }

        private void txtWriteData_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
           ReadFile();

            readThread = new Thread(ReadFile);
            readThread.Start();

          


        }

        private  void ReadFile()
        {
            if (txtReadFile.Text.Length < 5)
            {
                MessageBox.Show("No File Selected yet");
                return;
            }
            FileStream fs = null;
            StreamReader sr = null;

            try
            {
                string readFile = txtReadFile.Text;
                fs = new FileStream(readFile, FileMode.Open, FileAccess.Read,
                FileShare.ReadWrite);
                sr = new StreamReader(fs);

                string line = "";

                while ((line = sr.ReadLine()) != null)
                {
                    listboxRead.Items.Add(line);
                   
                }

            

            }

            catch (FileNotFoundException notfound)
            {
                Console.WriteLine(notfound.Message);
            }
            catch (IOException ioEx)
            {
                Console.WriteLine(ioEx.Message);
            }
            catch (UnauthorizedAccessException unauthEx)
            {
                Console.WriteLine(unauthEx.Message);
            }

            finally
            {
                sr.Close();
                fs.Close();

            }
        }

        private void btnBrowseRead_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtReadFile.Text = openFileDialog1.FileName;
            }
        }

        private void btnWrite_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtWriteData.Text = openFileDialog1.FileName;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            WriteFile();

            writeThread = new Thread(WriteFile);
            writeThread.Start();
        }

        private void WriteFile()
        {
            

            if (txtWriteData.Text.Length < 5  )
            {
                MessageBox.Show("No File Selected yet");
                return;
            }
            FileStream fs = null;
            StreamWriter sw = null;
            try
            {
                string fname = txtWriteData.Text;

                 //fn = txtWriteData.Text;

                fs = new FileStream(fname, FileMode.OpenOrCreate, FileAccess.ReadWrite,
                    FileShare.Read);

                sw = new StreamWriter(fs);

                for (int i = 0; i <= 10000; i++)
                {
                    sw.WriteLine("Welcome To Tricentis");
                   
                }

                

            }
            catch (FileNotFoundException notfound)
            {
                Console.WriteLine(notfound.Message);

            }
            catch (IOException ioEx)
            {
                Console.WriteLine(ioEx.Message);
            }
            catch (UnauthorizedAccessException unauthEx)
            {
                Console.WriteLine(unauthEx.Message);
            }
            finally
            {
                sw.Close();
                fs.Close();

            }

        }

        private void listboxRead_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
