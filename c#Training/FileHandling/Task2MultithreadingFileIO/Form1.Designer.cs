﻿namespace Task2MultithreadingFileIO
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnBrowseRead = new System.Windows.Forms.Button();
            this.listboxRead = new System.Windows.Forms.ListBox();
            this.txtReadFile = new System.Windows.Forms.TextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.txtWriteData = new System.Windows.Forms.TextBox();
            this.listBoxWrite = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnBrowseRead
            // 
            this.btnBrowseRead.Location = new System.Drawing.Point(12, 22);
            this.btnBrowseRead.Name = "btnBrowseRead";
            this.btnBrowseRead.Size = new System.Drawing.Size(131, 40);
            this.btnBrowseRead.TabIndex = 9;
            this.btnBrowseRead.Text = "Browse Read File";
            this.btnBrowseRead.UseVisualStyleBackColor = true;
            this.btnBrowseRead.Click += new System.EventHandler(this.btnBrowseRead_Click);
            // 
            // listboxRead
            // 
            this.listboxRead.FormattingEnabled = true;
            this.listboxRead.ItemHeight = 16;
            this.listboxRead.Location = new System.Drawing.Point(12, 165);
            this.listboxRead.Name = "listboxRead";
            this.listboxRead.Size = new System.Drawing.Size(474, 436);
            this.listboxRead.TabIndex = 8;
            // 
            // txtReadFile
            // 
            this.txtReadFile.Location = new System.Drawing.Point(149, 23);
            this.txtReadFile.Multiline = true;
            this.txtReadFile.Name = "txtReadFile";
            this.txtReadFile.Size = new System.Drawing.Size(337, 40);
            this.txtReadFile.TabIndex = 7;
            this.txtReadFile.TextChanged += new System.EventHandler(this.txtReadFile_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(545, 21);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 42);
            this.button1.TabIndex = 11;
            this.button1.Text = "Browse Write File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtWriteData
            // 
            this.txtWriteData.Location = new System.Drawing.Point(682, 22);
            this.txtWriteData.Multiline = true;
            this.txtWriteData.Name = "txtWriteData";
            this.txtWriteData.Size = new System.Drawing.Size(350, 40);
            this.txtWriteData.TabIndex = 10;
            // 
            // listBoxWrite
            // 
            this.listBoxWrite.FormattingEnabled = true;
            this.listBoxWrite.ItemHeight = 16;
            this.listBoxWrite.Location = new System.Drawing.Point(558, 165);
            this.listBoxWrite.Name = "listBoxWrite";
            this.listBoxWrite.Size = new System.Drawing.Size(474, 436);
            this.listBoxWrite.TabIndex = 12;
            this.listBoxWrite.SelectedIndexChanged += new System.EventHandler(this.listBoxWrite_SelectedIndexChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(450, 94);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 41);
            this.button2.TabIndex = 13;
            this.button2.Text = "Write Data";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1095, 707);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.listBoxWrite);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtWriteData);
            this.Controls.Add(this.btnBrowseRead);
            this.Controls.Add(this.listboxRead);
            this.Controls.Add(this.txtReadFile);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnBrowseRead;
        private System.Windows.Forms.ListBox listboxRead;
        private System.Windows.Forms.TextBox txtReadFile;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtWriteData;
        private System.Windows.Forms.ListBox listBoxWrite;
        private System.Windows.Forms.Button button2;
    }
}

