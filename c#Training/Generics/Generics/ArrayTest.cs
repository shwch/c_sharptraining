﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class ArrayTest<Test>
    {
        public Test[] myArr;
        public ArrayTest(int size)
        {
            myArr = new Test[size + 1];
        }
    }
}
