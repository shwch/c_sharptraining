﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class MyArray<T>
    {
        private T[] myArr;
         
        public MyArray(int size)
        {
            myArr = new T[size +1];
        }
        
        public T  GetItem(int index)
        {
            return myArr[index];
        }
        public void SetItem(int index, T value)
        {
            myArr[index] = value;
        }

        public void Display()
        {
            foreach (T val in myArr)
            {
                Console.WriteLine(val);
            }
        }


    }
}
