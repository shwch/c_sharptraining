﻿using Generics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generics
{
    class Program
    {
        static void Main1(string[] args)
        {
            MyArray<string> names = new MyArray<string>(5);
            names.SetItem(0,"Shweta");
            names.SetItem(1, "Pranita");
            names.SetItem(2, "Varsha");
            names.SetItem(3, "Kavita");
            names.SetItem(4, "Manisha");

            if (names.GetItem(2) == "Varsha")
            {
                names.SetItem(2, names.GetItem(2).ToUpper());
            }

            names.Display();
            Console.Read();

        }

        //private static void Main(string[] args)
        //{
        //    MyArray<int> phone = new MyArray<int>(5);
        //    phone.SetItem(1, 2345456);
        //    phone.SetItem(2,123456);
        //    phone.SetItem(3,69856);
        //    phone.SetItem(4,4589123);
        //    phone.SetItem(5,3654123);

        //    phone.Display();
        //    Console.Read();


        //}

        private static void Main2(string[] args)
        {
            MyArray<ArrayTest<string>> testarr = new MyArray<ArrayTest<string>>(5);
            testarr.SetItem(0,new ArrayTest<string>(3));
            testarr.GetItem(0).myArr[0] = "aaa";
            
        }

        private static void Main(string[] args)
        {
            int x = 10 , y =20;
            String name = "Shweta";
            String lastname = "Chitte";

            Swap(ref x,ref y);
            Swap(ref name , ref lastname);

            Console.WriteLine("X:" + x +" "+ "Y:" + y);
            Console.WriteLine("Name:" + name + " " + "Lastname:" + lastname);
            Console.ReadLine();

        }

        public static void Swap<T>(ref T val1,ref  T val2)
        {
            T tmp = val1;
            val1 = val2;
            val2 = tmp;
        }
    }
}
