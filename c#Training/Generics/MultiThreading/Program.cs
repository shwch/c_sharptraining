﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading
{
    class Program
    {
        static void Main(string[] args)
        {
           Console.WriteLine("App starts");
            Thread t1= new Thread(fun1);
            Thread t2 = new Thread(fun2);
            Thread t3 = new Thread(()=>fun3("33333333333..........."));


            t1.IsBackground = true;
            t2.IsBackground = true;
            t3.IsBackground = true;

            t1.Start();
            t2.Start();
            t3.Start();

            Console.WriteLine("App ends");
            
           //Console.ReadLine() // bcz main waits for input so do start without debugging.

        }

        private static void fun1()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("11111111111111...........");
                Thread.Sleep(300);
            }
        }
        private static void fun2()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("2222222222222222222...........");
                Thread.Sleep(300);

            }
        }

        private static void fun3(string data)
        {

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(data);
                Thread.Sleep(300);
            }
           
        }
    }
}
