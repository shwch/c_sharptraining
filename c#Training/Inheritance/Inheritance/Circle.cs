﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Circle:Shape
    {
        int rad;
        public Circle(string name,int rad):
            base(name)
        {
            this.rad = rad;
        }

        public override double CalArea()
        {
            return 22.7 *rad * rad;
        }
        public override string GetShapeInfo()
        {
            return base.GetShapeInfo() + " Circle \nHas radius: "+rad;
        }
    }
}
