﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Program
    {
        public static void Main(string[] args)
        {

            List<Shape> shapeList = new List<Shape>(); // since shape is abstract so we cannot make ita object
            shapeList.Add(new Circle("Circle",4));
            shapeList.Add(new Rectangle("Rectangle",10,12));
            shapeList.Add(new Square("Square",5));
            shapeList.Add(new Triangle("Triangle",2,6));

            foreach (Shape s in shapeList)
            {
                Console.WriteLine(s.GetShapeInfo()+ "\nArea is:"+ s.CalArea()+ "\n");
            }
            Console.ReadLine();
        }



        static void Main1(string[] args)
        {
            
            Shape[] arr = new Shape[4]; //array has fixed size so making list
            arr[0] = new Circle("Circle", 5);
            arr[1] = new Square("Square", 5);
            arr[2] = new Rectangle("Rectangle",2,6);
            arr[3] = new Triangle("Tringle",4, 5);
            foreach(Shape s in arr)
            {
                Console.WriteLine(s.GetShapeInfo());
                Console.WriteLine(" Area is: " + s.CalArea());
            }
            
            Console.ReadLine();
        }
    }
}
