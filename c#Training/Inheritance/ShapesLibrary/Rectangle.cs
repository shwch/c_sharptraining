﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapesLibrary
{
    public class Rectangle:Shape
    {
        int breadth;
        int length;
        public Rectangle(string name, int breadth,int length):base(name)
        {
            this.breadth = breadth;
            this.length = length;
        }

        public override double CalArea()
        {
            return (breadth * length) ;
        }
        public override string GetShapeInfo()
        {
            return base.GetShapeInfo() + " Rectangle.\nHas breadth: " + breadth +" and length: "+ length;
        }
    }
}
