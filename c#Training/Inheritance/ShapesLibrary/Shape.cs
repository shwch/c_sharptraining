﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapesLibrary
{
  public abstract  class Shape
    {
        string name;

        public Shape(string name)
        {
            this.name = name;
        }

        public virtual string GetShapeInfo()
        {
            return "Shape is: ";
        }
        //public virtual double CalArea()
        //{
        //    return 0;
        //}

        public abstract double CalArea();
    }
}
