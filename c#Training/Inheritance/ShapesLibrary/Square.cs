﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapesLibrary
{
    public class Square:Shape
    {
        int side;
        public Square(string name, int side):base(name)
        {
            this.side = side;
        }

        public override double CalArea()
        {
            return side * side;
        }
        public override string GetShapeInfo()
        {
            return base.GetShapeInfo() + " Square.\nHas side " + side;
        }
    }
}
