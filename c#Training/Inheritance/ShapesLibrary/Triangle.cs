﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapesLibrary
{
  public class Triangle:Shape
    {
        int breadth;
        int height;
        public Triangle(string name, int breadth, int height):base(name)
        {
            this.breadth = breadth;
            this.height = height;
        }

        public override double CalArea()
        {
            return (breadth * height) / 2;
        }
        public override string GetShapeInfo()
        {
            return base.GetShapeInfo() + " Triangle.\nHas breadth: " + breadth + " and height: " + height;
        }
    }
}
