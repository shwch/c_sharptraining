﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MutiThreadingWinforms
{
    public partial class Form1 : Form
    {
        private int progress = 0;
        int index = -1;
        List<Image> Imranimages;
        List<Image> Priyankaimages;
        Thread I1;
        Thread P1;
        public Form1()
        {
            InitializeComponent();
            


            Imranimages = new List<Image>();
            DirectoryInfo di = new DirectoryInfo(@"G:\Imran Hashmi\");
            FileInfo[] finfos = di.GetFiles("*.jpg", SearchOption.AllDirectories);
            foreach (FileInfo fi in finfos)
            Imranimages.Add(Image.FromFile(fi.FullName));

            Priyankaimages = new List<Image>();
            DirectoryInfo dp = new DirectoryInfo(@"G:\Priyanka Chopra\");
            FileInfo[] Pfinfos = dp.GetFiles("*.jpg", SearchOption.AllDirectories);
            foreach (FileInfo pfi in Pfinfos)
            Priyankaimages.Add(Image.FromFile(pfi.FullName));

            

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            

        }

        private async void btnStart_Click(object sender, EventArgs e)
        {
            I1 = new Thread(ImranThread);
            I1.Start();

            
            P1 = new Thread(PriyankaThread);
            P1.Start();
            
            //enable and set timer

            timer1.Enabled = true;
            timer1.Interval = 50;
        }

        private  void PriyankaThread()
        {
            for (index = -1; index <5; index++)
            {
                //index++;
                if (index < 0 || index >= Priyankaimages.Count)
                    index = 0;
                pictureBox2.Image = Priyankaimages[index];
                Thread.Sleep(300);

            }

            
        }

        private void ImranThread()
        {
            for (index = -1; index < 5; index++)
            {
                //index++;
                if (index < 0 || index >= Imranimages.Count)
                    index = 0;
                pictureBox1.Image = Imranimages[index];
                Thread.Sleep(300);
                
            }

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            //stop timer
            timer1.Stop();
            //update button text

            if (0 < progress && progress < 100)
            {
                btnStart.Text = "Resume";
            }

            I1.Abort();
            P1.Abort();
           
        }

        private void txtPercent_TextChanged(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        { 
            //increment progress

            progress += 1;

            if (progress >= 100)
            {
                timer1.Enabled = false;
                timer1.Stop();
            }
            //continue increment progress

            progressBar1.Value = progress;
            txtPercent.Text = progress.ToString();


        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            //reset 
            progress = 0;
            progressBar1.Value = 0;
            txtPercent.Text = "0.00";
        }
    }
}
