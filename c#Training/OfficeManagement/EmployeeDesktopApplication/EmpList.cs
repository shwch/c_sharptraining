﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    public static class EmpList
    {
        private static Emp[] elist;

        static EmpList()
        {
            elist = new Emp[3]; // 3 refrerences are created 
        }

        public static void AddEmployees()
        {
            elist[0] = new Emp(111,"Shweta",10000); // here actual objects are called on heap
            elist[1] = new Emp(112,"Manisha",20000);
            elist[2] = new Emp(113,"Kavita",30000);
            
        }

        //public void hello()  // cannot create   a nonstatic method in static class.
        //{
        //}

        public static Emp[] Getemployees()
        {
            return elist;
        }
    }
}
