﻿namespace EmployeeDesktopApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblID = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblSalary = new System.Windows.Forms.Label();
            this.tetID = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtSalary = new System.Windows.Forms.TextBox();
            this.lblTypeOfEmployee = new System.Windows.Forms.Label();
            this.comboTypeOfEmployee = new System.Windows.Forms.ComboBox();
            this.grpBoxEngineer = new System.Windows.Forms.GroupBox();
            this.txtIcentives = new System.Windows.Forms.TextBox();
            this.txtExtrahrs = new System.Windows.Forms.TextBox();
            this.lblIncentive = new System.Windows.Forms.Label();
            this.lblExtrahrs = new System.Windows.Forms.Label();
            this.grpBoxManager = new System.Windows.Forms.GroupBox();
            this.txtBonus = new System.Windows.Forms.TextBox();
            this.txtDept = new System.Windows.Forms.TextBox();
            this.lblBonus = new System.Windows.Forms.Label();
            this.lblDept = new System.Windows.Forms.Label();
            this.grpBoxSalesPerson = new System.Windows.Forms.GroupBox();
            this.txtComm = new System.Windows.Forms.TextBox();
            this.txtSales = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblSales = new System.Windows.Forms.Label();
            this.btnAddEmployee = new System.Windows.Forms.Button();
            this.btnShowAllEmployee = new System.Windows.Forms.Button();
            this.btnEmloyeeCount = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.grpBoxEngineer.SuspendLayout();
            this.grpBoxManager.SuspendLayout();
            this.grpBoxSalesPerson.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(47, 38);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(21, 17);
            this.lblID.TabIndex = 0;
            this.lblID.Text = "ID";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(47, 89);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(45, 17);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Name";
            // 
            // lblSalary
            // 
            this.lblSalary.AutoSize = true;
            this.lblSalary.Location = new System.Drawing.Point(47, 145);
            this.lblSalary.Name = "lblSalary";
            this.lblSalary.Size = new System.Drawing.Size(48, 17);
            this.lblSalary.TabIndex = 2;
            this.lblSalary.Text = "Salary";
            // 
            // tetID
            // 
            this.tetID.Location = new System.Drawing.Point(135, 35);
            this.tetID.Name = "tetID";
            this.tetID.Size = new System.Drawing.Size(160, 22);
            this.tetID.TabIndex = 3;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(135, 86);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(160, 22);
            this.txtName.TabIndex = 4;
            // 
            // txtSalary
            // 
            this.txtSalary.Location = new System.Drawing.Point(135, 142);
            this.txtSalary.Name = "txtSalary";
            this.txtSalary.Size = new System.Drawing.Size(160, 22);
            this.txtSalary.TabIndex = 5;
            // 
            // lblTypeOfEmployee
            // 
            this.lblTypeOfEmployee.AutoSize = true;
            this.lblTypeOfEmployee.Location = new System.Drawing.Point(12, 185);
            this.lblTypeOfEmployee.Name = "lblTypeOfEmployee";
            this.lblTypeOfEmployee.Size = new System.Drawing.Size(122, 17);
            this.lblTypeOfEmployee.TabIndex = 6;
            this.lblTypeOfEmployee.Text = "Type of Employee";
            // 
            // comboTypeOfEmployee
            // 
            this.comboTypeOfEmployee.FormattingEnabled = true;
            this.comboTypeOfEmployee.Items.AddRange(new object[] {
            "Manager",
            "Engineer",
            "SalesPerson"});
            this.comboTypeOfEmployee.Location = new System.Drawing.Point(135, 182);
            this.comboTypeOfEmployee.Name = "comboTypeOfEmployee";
            this.comboTypeOfEmployee.Size = new System.Drawing.Size(160, 24);
            this.comboTypeOfEmployee.TabIndex = 7;
            this.comboTypeOfEmployee.Text = "Select one";
            this.comboTypeOfEmployee.SelectedIndexChanged += new System.EventHandler(this.comboTypeOfEmployee_SelectedIndexChanged);
            // 
            // grpBoxEngineer
            // 
            this.grpBoxEngineer.Controls.Add(this.txtIcentives);
            this.grpBoxEngineer.Controls.Add(this.txtExtrahrs);
            this.grpBoxEngineer.Controls.Add(this.lblIncentive);
            this.grpBoxEngineer.Controls.Add(this.lblExtrahrs);
            this.grpBoxEngineer.Location = new System.Drawing.Point(25, 238);
            this.grpBoxEngineer.Name = "grpBoxEngineer";
            this.grpBoxEngineer.Size = new System.Drawing.Size(280, 119);
            this.grpBoxEngineer.TabIndex = 8;
            this.grpBoxEngineer.TabStop = false;
            this.grpBoxEngineer.Text = "Engineer";
            this.grpBoxEngineer.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txtIcentives
            // 
            this.txtIcentives.Location = new System.Drawing.Point(90, 61);
            this.txtIcentives.Name = "txtIcentives";
            this.txtIcentives.Size = new System.Drawing.Size(99, 22);
            this.txtIcentives.TabIndex = 15;
            // 
            // txtExtrahrs
            // 
            this.txtExtrahrs.Location = new System.Drawing.Point(90, 30);
            this.txtExtrahrs.Name = "txtExtrahrs";
            this.txtExtrahrs.Size = new System.Drawing.Size(99, 22);
            this.txtExtrahrs.TabIndex = 14;
            // 
            // lblIncentive
            // 
            this.lblIncentive.AutoSize = true;
            this.lblIncentive.Location = new System.Drawing.Point(7, 64);
            this.lblIncentive.Name = "lblIncentive";
            this.lblIncentive.Size = new System.Drawing.Size(64, 17);
            this.lblIncentive.TabIndex = 1;
            this.lblIncentive.Text = "Incentive";
            // 
            // lblExtrahrs
            // 
            this.lblExtrahrs.AutoSize = true;
            this.lblExtrahrs.Location = new System.Drawing.Point(7, 31);
            this.lblExtrahrs.Name = "lblExtrahrs";
            this.lblExtrahrs.Size = new System.Drawing.Size(66, 17);
            this.lblExtrahrs.TabIndex = 0;
            this.lblExtrahrs.Text = "Extra Hrs";
            // 
            // grpBoxManager
            // 
            this.grpBoxManager.Controls.Add(this.txtBonus);
            this.grpBoxManager.Controls.Add(this.txtDept);
            this.grpBoxManager.Controls.Add(this.lblBonus);
            this.grpBoxManager.Controls.Add(this.lblDept);
            this.grpBoxManager.Location = new System.Drawing.Point(301, 237);
            this.grpBoxManager.Name = "grpBoxManager";
            this.grpBoxManager.Size = new System.Drawing.Size(280, 119);
            this.grpBoxManager.TabIndex = 9;
            this.grpBoxManager.TabStop = false;
            this.grpBoxManager.Text = "Manager";
            // 
            // txtBonus
            // 
            this.txtBonus.Location = new System.Drawing.Point(67, 61);
            this.txtBonus.Name = "txtBonus";
            this.txtBonus.Size = new System.Drawing.Size(99, 22);
            this.txtBonus.TabIndex = 17;
            // 
            // txtDept
            // 
            this.txtDept.Location = new System.Drawing.Point(67, 32);
            this.txtDept.Name = "txtDept";
            this.txtDept.Size = new System.Drawing.Size(99, 22);
            this.txtDept.TabIndex = 16;
            // 
            // lblBonus
            // 
            this.lblBonus.AutoSize = true;
            this.lblBonus.Location = new System.Drawing.Point(7, 64);
            this.lblBonus.Name = "lblBonus";
            this.lblBonus.Size = new System.Drawing.Size(48, 17);
            this.lblBonus.TabIndex = 1;
            this.lblBonus.Text = "Bonus";
            // 
            // lblDept
            // 
            this.lblDept.AutoSize = true;
            this.lblDept.Location = new System.Drawing.Point(7, 31);
            this.lblDept.Name = "lblDept";
            this.lblDept.Size = new System.Drawing.Size(38, 17);
            this.lblDept.TabIndex = 0;
            this.lblDept.Text = "Dept";
            // 
            // grpBoxSalesPerson
            // 
            this.grpBoxSalesPerson.Controls.Add(this.txtComm);
            this.grpBoxSalesPerson.Controls.Add(this.txtSales);
            this.grpBoxSalesPerson.Controls.Add(this.label3);
            this.grpBoxSalesPerson.Controls.Add(this.lblSales);
            this.grpBoxSalesPerson.Location = new System.Drawing.Point(587, 237);
            this.grpBoxSalesPerson.Name = "grpBoxSalesPerson";
            this.grpBoxSalesPerson.Size = new System.Drawing.Size(280, 119);
            this.grpBoxSalesPerson.TabIndex = 9;
            this.grpBoxSalesPerson.TabStop = false;
            this.grpBoxSalesPerson.Text = "Sales Person";
            // 
            // txtComm
            // 
            this.txtComm.Location = new System.Drawing.Point(86, 62);
            this.txtComm.Name = "txtComm";
            this.txtComm.Size = new System.Drawing.Size(99, 22);
            this.txtComm.TabIndex = 19;
            // 
            // txtSales
            // 
            this.txtSales.Location = new System.Drawing.Point(86, 29);
            this.txtSales.Name = "txtSales";
            this.txtSales.Size = new System.Drawing.Size(99, 22);
            this.txtSales.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Comm";
            // 
            // lblSales
            // 
            this.lblSales.AutoSize = true;
            this.lblSales.Location = new System.Drawing.Point(7, 31);
            this.lblSales.Name = "lblSales";
            this.lblSales.Size = new System.Drawing.Size(43, 17);
            this.lblSales.TabIndex = 0;
            this.lblSales.Text = "Sales";
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.Location = new System.Drawing.Point(25, 363);
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Size = new System.Drawing.Size(175, 36);
            this.btnAddEmployee.TabIndex = 10;
            this.btnAddEmployee.Text = "Add Employee";
            this.btnAddEmployee.UseVisualStyleBackColor = true;
            // 
            // btnShowAllEmployee
            // 
            this.btnShowAllEmployee.Location = new System.Drawing.Point(301, 363);
            this.btnShowAllEmployee.Name = "btnShowAllEmployee";
            this.btnShowAllEmployee.Size = new System.Drawing.Size(175, 36);
            this.btnShowAllEmployee.TabIndex = 11;
            this.btnShowAllEmployee.Text = "Show All Employee";
            this.btnShowAllEmployee.UseVisualStyleBackColor = true;
            // 
            // btnEmloyeeCount
            // 
            this.btnEmloyeeCount.Location = new System.Drawing.Point(587, 362);
            this.btnEmloyeeCount.Name = "btnEmloyeeCount";
            this.btnEmloyeeCount.Size = new System.Drawing.Size(175, 36);
            this.btnEmloyeeCount.TabIndex = 12;
            this.btnEmloyeeCount.Text = "Emloyee Count";
            this.btnEmloyeeCount.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(25, 428);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(737, 292);
            this.listBox1.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1131, 743);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.btnEmloyeeCount);
            this.Controls.Add(this.btnShowAllEmployee);
            this.Controls.Add(this.btnAddEmployee);
            this.Controls.Add(this.grpBoxSalesPerson);
            this.Controls.Add(this.grpBoxManager);
            this.Controls.Add(this.grpBoxEngineer);
            this.Controls.Add(this.comboTypeOfEmployee);
            this.Controls.Add(this.lblTypeOfEmployee);
            this.Controls.Add(this.txtSalary);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.tetID);
            this.Controls.Add(this.lblSalary);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblID);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpBoxEngineer.ResumeLayout(false);
            this.grpBoxEngineer.PerformLayout();
            this.grpBoxManager.ResumeLayout(false);
            this.grpBoxManager.PerformLayout();
            this.grpBoxSalesPerson.ResumeLayout(false);
            this.grpBoxSalesPerson.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblSalary;
        private System.Windows.Forms.TextBox tetID;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtSalary;
        private System.Windows.Forms.Label lblTypeOfEmployee;
        private System.Windows.Forms.ComboBox comboTypeOfEmployee;
        private System.Windows.Forms.GroupBox grpBoxEngineer;
        private System.Windows.Forms.Label lblIncentive;
        private System.Windows.Forms.Label lblExtrahrs;
        private System.Windows.Forms.GroupBox grpBoxManager;
        private System.Windows.Forms.Label lblBonus;
        private System.Windows.Forms.Label lblDept;
        private System.Windows.Forms.GroupBox grpBoxSalesPerson;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblSales;
        private System.Windows.Forms.Button btnAddEmployee;
        private System.Windows.Forms.Button btnShowAllEmployee;
        private System.Windows.Forms.Button btnEmloyeeCount;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox txtIcentives;
        private System.Windows.Forms.TextBox txtExtrahrs;
        private System.Windows.Forms.TextBox txtBonus;
        private System.Windows.Forms.TextBox txtDept;
        private System.Windows.Forms.TextBox txtComm;
        private System.Windows.Forms.TextBox txtSales;
    }
}

