﻿using OfficeManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmployeeDesktopApplication
{
    public partial class Form1 : Form
    {
        private List<Emp> elList;
        public Form1()
        {
            InitializeComponent();
            DisableGroupBoxes();

            elList = new List<Emp>();


        }


        private void DisableGroupBoxes()
        {
            grpBoxEngineer.Enabled = false;
            grpBoxManager.Enabled = false;
            grpBoxSalesPerson.Enabled = false;
        }


        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void comboTypeOfEmployee_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisableGroupBoxes();
            if (comboTypeOfEmployee.SelectedIndex ==0)
            {
                elList.Add(new Engineer(Convert.ToInt32(txtExtrahrs.Text));
            }

            else if (comboTypeOfEmployee.SelectedIndex == 1)
            {
                grpBoxManager.Enabled = true;
            }
            else
            {
                grpBoxSalesPerson.Enabled = true;
            }
        }
    }
}
