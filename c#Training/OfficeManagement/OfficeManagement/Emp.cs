﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
   public class Emp
    {
        private int id;
        public string name;
        private double salary;
        static int count;
        static string companyName ;


        static Emp()
        {
            Console.WriteLine("static Constructor called");
            count = 100;
            companyName = "Tricentis";
        }

        public Emp()
        {
           Console.WriteLine("Default Constructor called");
            this.id = 1000;
            this.name = "Ravi";
            this.salary = 5000;
            count++;
        }
        public Emp(int id, string name, int salary)
        {
            Console.WriteLine("Parameterised Constructor called");
            this.id=id;
            this.name =name ;
            this.salary=salary;
            count++;
        }

        public Emp(Emp e )
        {
            Console.WriteLine("Copy Constructor called");
            this.id = e.id;
            this.name = e.name;
            this.salary = e.salary;
            count++;
        }

        public Emp(string lname, int eid)
        {
            Console.WriteLine("Constructor called");
            this.id = eid;
            this.name = lname;
           

        }

        public void SetEmpInfo(int id ,string nm , double sal)
        {
            //authentication code
          this.id = id; //this means id of class
          this.name = nm;  // this.name = nm will also work. this is  a reference of object that calls a function
          this.salary = sal;
        }

        public string GetEmpInfo()
        {
            return "Employee info is:" + " " +this.id + " " + this.name + " "+this.salary + " " + companyName;

        }

        public double CalSal(int increment)
        {
            return salary += increment;
        }

        public static int GetCount()
        {
            return count; //made count private and method as public for security reasons
        }

        //public static string GetCompanyName()
        //{
        //    return companyName;
            
        //}

        //public static void SetCompanyName( string cmp)
        //{
        //    companyName = cmp;

        //}



    }
}
