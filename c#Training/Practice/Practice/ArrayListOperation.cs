﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    class ArrayListOperation
    {
        public static int Addition()
        {
            ArrayList list = new ArrayList();
            list.Add(5);
            list.Add(10);

            int result = 0;

            foreach (int value in list)
            {
                result += value;
            }
            return result;
        }
    }
}
