﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    public class Fnood : GenericSample<Chair>
    {
        public override string Title
        {
            get { return "Fnood"; }
        }

        public override string Colour
        {
            get { return "Cyan"; }
        }
    }
}
