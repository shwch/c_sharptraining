﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    public abstract class GenericSample<TContents> where TContents : IToolKit, IParts, new()
    {
        public abstract string Title
        {
            get;
        }

        public abstract string Colour
        {
            get;
        }
        public void GetInventory()
        {
            // generic constraint new() lets me do this
            var contents = new TContents();
            foreach (string tool in contents.GetTools())
            {
                Console.WriteLine("Tool: {0}", tool);
            }
            foreach (string part in contents.GetParts())
            {
                Console.WriteLine("Part: {0}", part);
            }
        }

        // describes a chair
        public class Chair : IToolKit, IParts
        {
            public string[] GetTools()
            {
                return new string[] { "Screwdriver", "Allen Key" };
            }

            public string[] GetParts()
            {
                return new string[] {
            "leg", "leg", "leg", "seat", "back", "bag of screws" };
            }
        }

        // describes a chair kit call "Fnood" which is cyan in colour.
        
    }
}
