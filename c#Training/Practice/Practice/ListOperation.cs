﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    class ListOperation
    {
        public static int Addition()
        {
            List<int> list = new List<int>();
            list.Add(5);
            list.Add(10);

            int result = 0;

            foreach (int  value in list)
            {
                result += value;
            }

            return result;
        }

    }
}
