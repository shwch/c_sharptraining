﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    class Performance
    {
       public static void NonGenericPerformance()
        {
            long operationTime = 0;
            ArrayList arrayList = new ArrayList();
            Stopwatch sw = new Stopwatch();

            sw.Start();

            for (int i = 1; i <= 100000; i++)
            {
                arrayList.Add(i);
            }

            operationTime = sw.ElapsedMilliseconds;

            Console.WriteLine("Array List{0} values add time is{1} milliseconds", arrayList.Count, operationTime);
            sw.Restart();
            foreach (int i in arrayList)
            {
                int value = i;
            }
            operationTime = sw.ElapsedMilliseconds;
            Console.WriteLine("Array List{0} values retrieve time is {1} milliseconds", arrayList.Count, operationTime);

        }

       public static void GenericPerformance()
        {
            long operationTime = 0;

            List<int> list = new List<int>();

            Stopwatch sw = new Stopwatch();

            sw.Start();

            for (int i = 1; i <= 100000; i++)

            {

                list.Add(i);

            }

            operationTime = sw.ElapsedMilliseconds;

            Console.WriteLine("List {0} values add time is {1} milliseconds", list.Count, operationTime);

            sw.Restart();

            foreach (int i in list)

            {

                int value = i;

            }

            operationTime = sw.ElapsedMilliseconds;

            Console.WriteLine("List {0} values retrieve time is {1} milliseconds", list.Count, operationTime);
        }


    }
}
