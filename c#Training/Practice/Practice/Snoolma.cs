﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice
{
    public class Snoolma : GenericSample<Chair>
    {
        public override string Title
        {
            get { return "Snoolma "; }
        }

        public override string Colour
        {
            get { return "Orange"; }
        }
    }
}
