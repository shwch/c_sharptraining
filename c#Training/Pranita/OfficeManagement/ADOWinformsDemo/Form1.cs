﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ADOWinformsDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=TRI-NB0546;Initial Catalog=EmployeeDB;Integrated Security=True");
            SqlCommand cmd = new SqlCommand("select * from EmployeeInfo",conn); // handle exceptions every place.

            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                 MessageBox.Show("Emp info = " + sdr[0] +" "+ sdr["Name"]+ " "+sdr.GetDecimal(2)+" "+ sdr.GetString(3)); // can retireve using column no or name or type
                
            }

            conn.Close(); // this should come in finally.
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=TRI-NB0546;Initial Catalog=EmployeeDB;Integrated Security=True");
            SqlCommand cmd = new SqlCommand("select * from EmployeeInfo", conn); // handle exceptions every place.

            conn.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            dataGridView1.DataSource = sdr;

            // SqlDataAdapter Adp = new SqlDataAdapter("select * from EmployeeInfo", conn);

            /*DataTable Dt = new DataTable();

            Dt.Load(sdr);
            dataGridView1.DataSource = Dt; */  
            
            //this OneWayAttribute or binding way
           
            BindingSource bs =new BindingSource();
            bs.DataSource = sdr;

            dataGridView1.DataSource = bs;
            conn.Close(); // this should come in finally.
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=TRI-NB0546;Initial Catalog=EmployeeDB;Integrated Security=True"); // here it returns a single value from query so we use Execute Scalar
            SqlCommand cmd = new SqlCommand("select count(*) from EmployeeInfo", conn); // handle exceptions every place.

            conn.Open();
            int count = Convert.ToInt32(cmd.ExecuteScalar());

            MessageBox.Show("Employee count :" + count);
            conn.Close(); // this should come in finally.
        }
    }
}
