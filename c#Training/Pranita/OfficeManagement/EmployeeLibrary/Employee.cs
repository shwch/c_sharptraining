﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    class Employee
    {
        int id;
        string name;
        protected double salary; // to acess it in all derived class
        static int count;
        static string companyName;

        static Employee()
        {
            Console.WriteLine("static ctor called..");
            count = 200;
            companyName = "Tricentis India Engineering...";
        }
        public Employee()
        {
            Console.WriteLine("Def ctor called..");
            id = 100;
            name = "Chavan";
            salary = 2500;
            count++;
           
        }
        public Employee(int id,string nm,double sal)
        {
            Console.WriteLine("para ctor called..");
            this.id = id;
            name = nm;
            salary = sal;
            count++;

        }

        public Employee(string nm,int id, double sal)
        {
            Console.WriteLine("para ctor called..");
            this.id = id;
            name = nm;
            salary = sal;
        }

        public Employee(Employee e)
        {
            Console.WriteLine("copy ctor called..");
            id = e.id;
            name = e.name;
            salary =e.salary;
            count++;

        }

        public void SetEmpInfo(int id, string nm,double sal)
        {
            // here we can write authentication code 
            this.id = id; // avoid ambiguity for id var
            name = nm;
            salary = sal;
        }

        public virtual string GetEmpInfo()
        {
            return "Emp info: " + id + " " + name + " " + salary +" "+ companyName;
        }

        public static int GetCount()
        {
            return count;
        }
        public static void SetCompanyName(string comName)
        {
            companyName = comName;
        }
        public static string GetCompanyNAme()
        {
            return companyName;
        }

        public virtual double CalSal()
        {
            return salary;
        }

        public void  GivevInc(double inc)
        {
            this.salary += inc;
        }
    }
}
