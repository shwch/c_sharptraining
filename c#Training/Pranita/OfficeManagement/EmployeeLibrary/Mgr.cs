﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    class Mgr:Employee
    {
        string dept;
        double bonus;

        public Mgr(int id, string nm, double sal, string dept, double bonus):base(id,nm,sal)
        {
            this.dept = dept;
            this.bonus = bonus;

        }

        public override string GetEmpInfo()
        //public new string GetEmpInfo()
        {
            return base.GetEmpInfo() + dept + " " + bonus;
        }

        //public new double CalSal()
        public override double CalSal()
        {
            return base.CalSal() + bonus;
        }
    }
}
