﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    static class EmpList
    {
        static Employee[] emplist;

        static EmpList()
        {
            emplist = new Employee[3];
        }

       public  static void AddEmployee()
        {
            emplist[0] = new Employee(10, "Gore", 5000);
            emplist[1] = new Employee(20, "Chavan", 5000);
            emplist[2] = new Employee(30, "Pranita", 5000);
        }

       public  static Employee[] GetEmployee()
        {
            return emplist;
        }
    }
}
