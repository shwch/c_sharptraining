﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
   public  class Engg:Employee
    {
        double extraHrs;
        double incen;

        public Engg(int id,string nm,double sal,double extrahrs,double incn):base(id,nm,sal)
        {
            this.extraHrs = extrahrs;
            this.incen = incn;
        }

        //public new string GetEmpInfo()
        public override string GetEmpInfo()
        {
            return base.GetEmpInfo() + extraHrs + " " + incen;
        }

        //public new double CalSal()
        public override double CalSal()
        {
            return base.CalSal() + (extraHrs * incen);
        }
    }
}
