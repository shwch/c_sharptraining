﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    class MyUtilClass
    {
        public int Add(int num1,int num2)
        {
            return num1+num2;
        }

        public int Add(int num1, int num2,int num3)
        {
            return num1 + num2 + num3;
        }

        public string Add(string str1, string str2)
        {
            return str1+" "+str2;
        }

        public int Add(int[] arr)
        {
            return arr.Sum();
        }
    }
}
