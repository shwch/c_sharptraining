﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    class Program
    {

        static void Main2(string[] args)
        {
            //Console.WriteLine("Count is: "+Employee.GetCount());
            Employee.SetCompanyName("Tricentis India");
            Employee e = new Employee();
            //e.SetEmpInfo(101, "Pranita", 1000);
            Console.WriteLine(e.GetEmpInfo());


            Employee e2 = new Employee();
            //e2.SetEmpInfo(102, "Chepa", 1000);
            //e2.GetEmpInfo();
            Console.WriteLine(e2.GetEmpInfo());

            Employee e3 = new Employee(12, "Gore", 55555);
            Console.WriteLine(e3.GetEmpInfo());

            Employee e4 = new Employee(e3);
            Console.WriteLine(e4.GetEmpInfo());

            Employee e5 = new Employee("Shweta", 150, 5000);
            Console.WriteLine(e5.GetEmpInfo());

            Console.WriteLine("Count is: " + Employee.GetCount());


            Console.ReadLine();
        }

        static void Main1(string[] args)
        {
            MyUtilClass cls = new MyUtilClass();
            int sum2 = cls.Add(10, 20);
            int sum3 = cls.Add(10, 20, 30);
            string result = cls.Add("Pranita", "Chavan");
            int arraySum = cls.Add(new int[] { 10, 20, 30, 40, 50 });
            Console.WriteLine("Sum of 2: " + sum2);
            Console.WriteLine("Sum of 3: " + sum3);
            Console.WriteLine("string : " + result);
            Console.WriteLine("sum of array: " + arraySum);
            Console.ReadLine();
        }

        static void Main3(string[] args)
        {

            EmpList.AddEmployee();
            Employee[] arr = EmpList.GetEmployee();

            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i].GetEmpInfo());
            }
            Console.ReadLine();

        }

        static void Main4(string[] args)
        {
            SalesPer sl = new SalesPer(101, "Panu", 2500, 10, 20);
            Engg eng = new Engg(202, "Gore", 2300, 20, 30);
            Mgr mg = new Mgr(303, "Chavan", 1500, "HR", 60);

            Console.WriteLine(sl.GetEmpInfo());
            Console.WriteLine("Total Sal: " + sl.CalSal());
            Console.WriteLine(eng.GetEmpInfo());
            Console.WriteLine("Total Sal: " + eng.CalSal());
            Console.WriteLine(mg.GetEmpInfo());
            Console.WriteLine("Total Sal: " + mg.CalSal());
            Console.ReadLine();
        }
        static void Main(string[] args)
        {
            //Employee e = new SalesPer(101, "Panu", 2500, 10, 20);
            //Console.WriteLine( e.GetEmpInfo());

            Employee[] arr = new Employee[3];
            arr[0]= new SalesPer(101, "Panu", 2500, 10, 20);
            arr[1]= new Engg(202, "Gore", 2300, 20, 30);
            arr[2]= new Mgr(303, "Chavan", 1500, "HR", 60);

            foreach(Employee e in arr)
            {
                Console.WriteLine(e.GetEmpInfo());
                Console.WriteLine("Total Sal: "+e.CalSal());
            }

            Console.ReadLine();
        }

        }
}
