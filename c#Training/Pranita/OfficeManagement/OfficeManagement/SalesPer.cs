﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeManagement
{
    public class SalesPer:Employee
    {
        int sales;
        double comm;

        public SalesPer(int id,string nm,double sal,int sales,double comm):base(id,nm,sal)
        {
            this.sales = sales;
            this.comm = comm;
        }

        //public new string  GetEmpInfo()
        public override string GetEmpInfo()
        {
            return base.GetEmpInfo() + sales + " " +comm;
        }

        //public new double CalSal()
        public override double CalSal()
        {
            return base.CalSal() + sales * comm;
        }
    }
}
