﻿namespace WinFormEmployee
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtSalary = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbTypes = new System.Windows.Forms.ComboBox();
            this.grpEngineer = new System.Windows.Forms.GroupBox();
            this.txtIncn = new System.Windows.Forms.TextBox();
            this.txtExtraHrs = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.grpMAnager = new System.Windows.Forms.GroupBox();
            this.txtBonus = new System.Windows.Forms.TextBox();
            this.txtDept = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.grpSales = new System.Windows.Forms.GroupBox();
            this.txtComm = new System.Windows.Forms.TextBox();
            this.txtSales = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnAddEmployee = new System.Windows.Forms.Button();
            this.btnShow = new System.Windows.Forms.Button();
            this.btnEmpCount = new System.Windows.Forms.Button();
            this.listShow = new System.Windows.Forms.ListBox();
            this.btnAddInFile = new System.Windows.Forms.Button();
            this.btnFromFile = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.grpEngineer.SuspendLayout();
            this.grpMAnager.SuspendLayout();
            this.grpSales.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(113, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(88, 109);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(86, 167);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Salary";
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(204, 48);
            this.txtId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(148, 30);
            this.txtId.TabIndex = 3;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(204, 109);
            this.txtName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(148, 30);
            this.txtName.TabIndex = 4;
            // 
            // txtSalary
            // 
            this.txtSalary.Location = new System.Drawing.Point(204, 164);
            this.txtSalary.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSalary.Name = "txtSalary";
            this.txtSalary.Size = new System.Drawing.Size(148, 30);
            this.txtSalary.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 230);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 25);
            this.label4.TabIndex = 6;
            this.label4.Text = "Type Of Employee";
            // 
            // cmbTypes
            // 
            this.cmbTypes.FormattingEnabled = true;
            this.cmbTypes.Items.AddRange(new object[] {
            "SalesPerson",
            "Engineer",
            "Manager"});
            this.cmbTypes.Location = new System.Drawing.Point(204, 230);
            this.cmbTypes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmbTypes.Name = "cmbTypes";
            this.cmbTypes.Size = new System.Drawing.Size(180, 33);
            this.cmbTypes.TabIndex = 7;
            this.cmbTypes.Text = "Select One";
            this.cmbTypes.SelectedIndexChanged += new System.EventHandler(this.cmbTypes_SelectedIndexChanged);
            // 
            // grpEngineer
            // 
            this.grpEngineer.Controls.Add(this.txtIncn);
            this.grpEngineer.Controls.Add(this.txtExtraHrs);
            this.grpEngineer.Controls.Add(this.label6);
            this.grpEngineer.Controls.Add(this.label5);
            this.grpEngineer.Location = new System.Drawing.Point(16, 287);
            this.grpEngineer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grpEngineer.Name = "grpEngineer";
            this.grpEngineer.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grpEngineer.Size = new System.Drawing.Size(330, 136);
            this.grpEngineer.TabIndex = 8;
            this.grpEngineer.TabStop = false;
            this.grpEngineer.Text = "Engineer";
            // 
            // txtIncn
            // 
            this.txtIncn.Location = new System.Drawing.Point(145, 87);
            this.txtIncn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtIncn.Name = "txtIncn";
            this.txtIncn.Size = new System.Drawing.Size(148, 30);
            this.txtIncn.TabIndex = 10;
            // 
            // txtExtraHrs
            // 
            this.txtExtraHrs.Location = new System.Drawing.Point(145, 35);
            this.txtExtraHrs.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtExtraHrs.Name = "txtExtraHrs";
            this.txtExtraHrs.Size = new System.Drawing.Size(148, 30);
            this.txtExtraHrs.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 87);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 25);
            this.label6.TabIndex = 8;
            this.label6.Text = "Incentives";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 35);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 25);
            this.label5.TabIndex = 7;
            this.label5.Text = "Extra Hrs";
            // 
            // grpMAnager
            // 
            this.grpMAnager.Controls.Add(this.txtBonus);
            this.grpMAnager.Controls.Add(this.txtDept);
            this.grpMAnager.Controls.Add(this.label7);
            this.grpMAnager.Controls.Add(this.label8);
            this.grpMAnager.Location = new System.Drawing.Point(372, 287);
            this.grpMAnager.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grpMAnager.Name = "grpMAnager";
            this.grpMAnager.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grpMAnager.Size = new System.Drawing.Size(338, 136);
            this.grpMAnager.TabIndex = 9;
            this.grpMAnager.TabStop = false;
            this.grpMAnager.Text = "Manager";
            // 
            // txtBonus
            // 
            this.txtBonus.Location = new System.Drawing.Point(143, 87);
            this.txtBonus.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBonus.Name = "txtBonus";
            this.txtBonus.Size = new System.Drawing.Size(148, 30);
            this.txtBonus.TabIndex = 10;
            // 
            // txtDept
            // 
            this.txtDept.Location = new System.Drawing.Point(143, 37);
            this.txtDept.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDept.Name = "txtDept";
            this.txtDept.Size = new System.Drawing.Size(148, 30);
            this.txtDept.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 87);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 25);
            this.label7.TabIndex = 8;
            this.label7.Text = "Bonus";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(38, 37);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 25);
            this.label8.TabIndex = 7;
            this.label8.Text = "Dept";
            // 
            // grpSales
            // 
            this.grpSales.Controls.Add(this.txtComm);
            this.grpSales.Controls.Add(this.txtSales);
            this.grpSales.Controls.Add(this.label9);
            this.grpSales.Controls.Add(this.label10);
            this.grpSales.Location = new System.Drawing.Point(740, 287);
            this.grpSales.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grpSales.Name = "grpSales";
            this.grpSales.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.grpSales.Size = new System.Drawing.Size(319, 136);
            this.grpSales.TabIndex = 10;
            this.grpSales.TabStop = false;
            this.grpSales.Text = "SalesPerson";
            // 
            // txtComm
            // 
            this.txtComm.Location = new System.Drawing.Point(126, 81);
            this.txtComm.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtComm.Name = "txtComm";
            this.txtComm.Size = new System.Drawing.Size(148, 30);
            this.txtComm.TabIndex = 10;
            // 
            // txtSales
            // 
            this.txtSales.Location = new System.Drawing.Point(126, 29);
            this.txtSales.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtSales.Name = "txtSales";
            this.txtSales.Size = new System.Drawing.Size(148, 30);
            this.txtSales.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(27, 81);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 25);
            this.label9.TabIndex = 8;
            this.label9.Text = "Comm";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(27, 34);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 25);
            this.label10.TabIndex = 7;
            this.label10.Text = "Sales";
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.ForeColor = System.Drawing.Color.Navy;
            this.btnAddEmployee.Location = new System.Drawing.Point(75, 533);
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Size = new System.Drawing.Size(240, 35);
            this.btnAddEmployee.TabIndex = 11;
            this.btnAddEmployee.Text = "Add Employee";
            this.btnAddEmployee.UseVisualStyleBackColor = true;
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            // 
            // btnShow
            // 
            this.btnShow.ForeColor = System.Drawing.Color.Red;
            this.btnShow.Location = new System.Drawing.Point(415, 533);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(240, 35);
            this.btnShow.TabIndex = 12;
            this.btnShow.Text = "Show All Employee";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // btnEmpCount
            // 
            this.btnEmpCount.ForeColor = System.Drawing.Color.Purple;
            this.btnEmpCount.Location = new System.Drawing.Point(782, 533);
            this.btnEmpCount.Name = "btnEmpCount";
            this.btnEmpCount.Size = new System.Drawing.Size(240, 35);
            this.btnEmpCount.TabIndex = 13;
            this.btnEmpCount.Text = "Employee Count";
            this.btnEmpCount.UseVisualStyleBackColor = true;
            this.btnEmpCount.Click += new System.EventHandler(this.btnEmpCount_Click);
            // 
            // listShow
            // 
            this.listShow.FormattingEnabled = true;
            this.listShow.ItemHeight = 25;
            this.listShow.Location = new System.Drawing.Point(37, 604);
            this.listShow.Name = "listShow";
            this.listShow.Size = new System.Drawing.Size(1043, 104);
            this.listShow.TabIndex = 14;
            this.listShow.SelectedIndexChanged += new System.EventHandler(this.listShow_SelectedIndexChanged);
            // 
            // btnAddInFile
            // 
            this.btnAddInFile.ForeColor = System.Drawing.Color.Navy;
            this.btnAddInFile.Location = new System.Drawing.Point(491, 43);
            this.btnAddInFile.Name = "btnAddInFile";
            this.btnAddInFile.Size = new System.Drawing.Size(240, 35);
            this.btnAddInFile.TabIndex = 15;
            this.btnAddInFile.Text = "Add Employee In File";
            this.btnAddInFile.UseVisualStyleBackColor = true;
            this.btnAddInFile.Click += new System.EventHandler(this.btnAddInFile_Click);
            // 
            // btnFromFile
            // 
            this.btnFromFile.ForeColor = System.Drawing.Color.Navy;
            this.btnFromFile.Location = new System.Drawing.Point(793, 46);
            this.btnFromFile.Name = "btnFromFile";
            this.btnFromFile.Size = new System.Drawing.Size(240, 35);
            this.btnFromFile.TabIndex = 16;
            this.btnFromFile.Text = "Show Data From File";
            this.btnFromFile.UseVisualStyleBackColor = true;
            this.btnFromFile.Click += new System.EventHandler(this.btnFromFile_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Red;
            this.button1.Location = new System.Drawing.Point(185, 440);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 35);
            this.button1.TabIndex = 17;
            this.button1.Text = "Browse";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(331, 445);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(498, 30);
            this.textBox1.TabIndex = 18;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1339, 766);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnFromFile);
            this.Controls.Add(this.btnAddInFile);
            this.Controls.Add(this.listShow);
            this.Controls.Add(this.btnEmpCount);
            this.Controls.Add(this.btnShow);
            this.Controls.Add(this.btnAddEmployee);
            this.Controls.Add(this.grpSales);
            this.Controls.Add(this.grpMAnager);
            this.Controls.Add(this.grpEngineer);
            this.Controls.Add(this.cmbTypes);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtSalary);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.grpEngineer.ResumeLayout(false);
            this.grpEngineer.PerformLayout();
            this.grpMAnager.ResumeLayout(false);
            this.grpMAnager.PerformLayout();
            this.grpSales.ResumeLayout(false);
            this.grpSales.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtSalary;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbTypes;
        private System.Windows.Forms.GroupBox grpEngineer;
        private System.Windows.Forms.TextBox txtIncn;
        private System.Windows.Forms.TextBox txtExtraHrs;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox grpMAnager;
        private System.Windows.Forms.TextBox txtBonus;
        private System.Windows.Forms.TextBox txtDept;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox grpSales;
        private System.Windows.Forms.TextBox txtComm;
        private System.Windows.Forms.TextBox txtSales;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnAddEmployee;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.Button btnEmpCount;
        private System.Windows.Forms.ListBox listShow;
        private System.Windows.Forms.Button btnAddInFile;
        private System.Windows.Forms.Button btnFromFile;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
    }
}

