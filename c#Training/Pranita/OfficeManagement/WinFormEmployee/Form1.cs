﻿using OfficeManagement;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormEmployee
{
    public partial class Form1 : Form
    {
        List<Employee> eList;
      
        public Form1()
        {
            InitializeComponent();
            DisableGroupBoxes();
            eList = new List<Employee>();
        }

        void DisableGroupBoxes()
        {
            grpEngineer.Enabled = false;
            grpSales.Enabled = false;
            grpMAnager.Enabled = false;
        }
        void EnableGroupBoxes()
        {
            grpEngineer.Enabled = true;
            grpSales.Enabled = true;
            grpMAnager.Enabled = true;
        }
        private void cmbTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisableGroupBoxes();
            if(cmbTypes.SelectedItem.ToString() == "SalesPerson")
            {
                grpSales.Enabled = true;
                //grpEngineer.Enabled = false;
                //grpMAnager.Enabled = false;
            }
            if (cmbTypes.SelectedItem.ToString() == "Engineer")
            {
                grpEngineer.Enabled = true;
                //grpSales.Enabled = false;
                //grpMAnager.Enabled = false;
            }
            if (cmbTypes.SelectedItem.ToString() == "Manager")
            {
                grpMAnager.Enabled = true;
                //grpEngineer.Enabled = false;
               // grpSales.Enabled = false;
            }
        }

        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            if (cmbTypes.SelectedItem.ToString() == "SalesPerson")
            {
                eList.Add(new SalesPer(int.Parse(txtId.Text), txtName.Text, double.Parse(txtSalary.Text), int.Parse(txtExtraHrs.Text), double.Parse(txtIncn.Text)));
            }
            if (cmbTypes.SelectedItem.ToString() == "Engineer")
            {
                eList.Add(new Engg(int.Parse(txtId.Text), txtName.Text, double.Parse(txtSalary.Text), double.Parse(txtExtraHrs.Text), double.Parse(txtIncn.Text)));

            }
            if (cmbTypes.SelectedItem.ToString() == "Manager")
            {
                eList.Add(new Mgr(int.Parse(txtId.Text), txtName.Text, double.Parse(txtSalary.Text), txtDept.Text, double.Parse(txtBonus.Text)));

            }
            MessageBox.Show("Employee Added..");
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            listShow.Items.Clear();
            foreach(Employee emp in eList)
            {
                listShow.Items.Add(emp.GetEmpInfo() + " Total Sal: " + emp.CalSal());
            }
        }

        private void btnEmpCount_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Employee count" + eList.Count);
            MessageBox.Show("Emp cnt: " + Employee.GetCount());
        }

        private void btnAddInFile_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length<5)
            {
                MessageBox.Show("No File Selected yet");
                return;
            }
            FileStream fs = null;
            StreamWriter sw = null;


            try
            {
                string fname = textBox1.Text;
                fs = new FileStream(fname, FileMode.OpenOrCreate, FileAccess.ReadWrite,
                    FileShare.Read);

                sw = new StreamWriter(fs);

             


                foreach (Employee emp in eList)
                {
                    sw.WriteLine(emp.GetEmpInfo()+ " "+ emp.CalSal());
                }

                MessageBox.Show("Added into File");


            }
            catch (FileNotFoundException notfound)
            {
                Console.WriteLine(notfound.Message);
            }
            catch (IOException ioEx)
            {
                Console.WriteLine(ioEx.Message);
            }
            catch (UnauthorizedAccessException unauthEx)
            {
                Console.WriteLine(unauthEx.Message);
            }
            finally
            {
                sw.Close();
                fs.Close();

            }
        }

        private void btnFromFile_Click(object sender, EventArgs e)
        {
            FileStream fs = null;
            StreamReader sr = null;
            try
            {
                string fname = textBox1.Text;
                listShow.Items.Clear();
                fs = new FileStream(fname, FileMode.Open, FileAccess.Read,
                FileShare.Read);
                sr = new StreamReader(fs);
                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    listShow.Items.Add(line);
                }

            }
            catch (FileNotFoundException notfound)
            {
                Console.WriteLine(notfound.Message);
            }
            catch (IOException ioEx)
            {
                Console.WriteLine(ioEx.Message);
            }
            catch (UnauthorizedAccessException unauthEx)
            {
                Console.WriteLine(unauthEx.Message);
            }

            finally
            {
                sr.Close();
                fs.Close();

            }
        }

        private void listShow_SelectedIndexChanged(object sender, EventArgs e)
        {
            
           
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.Enabled = false;
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }
    }
}
