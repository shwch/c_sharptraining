﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingSite
{
    public static class ProductList
    {
         private static Shopper_Choice[] plist;
        static ProductList()
        {
            plist = new Shopper_Choice[3];
        }

        public static void AddProducts()
        {
            plist[0] = new Shopper_Choice("Saree", 3000);
            plist[1] = new Shopper_Choice("Dress", 2000);
            plist[2] = new Shopper_Choice("kurti", 1000);
        }

        public static Shopper_Choice[] GetProducts()
        {
            return plist;
        }

    }
}
