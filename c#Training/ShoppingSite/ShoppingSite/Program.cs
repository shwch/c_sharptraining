﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingSite
{
    class Program
    {
        static void Main(string[] args)
        {
            ProductList.AddProducts();

            Shopper_Choice[] productList = ProductList.GetProducts();
            foreach (Shopper_Choice sp in productList)
            {
                Console.WriteLine(sp.GetProductInfo());
            }
            Console.ReadKey();

            Console.ReadLine();
        }
    }
}
