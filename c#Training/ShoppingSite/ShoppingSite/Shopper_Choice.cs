﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingSite
{
    public class Shopper_Choice
    {
        static int productId =0;
        public string productName;
        double productPrice;
        public static string MallName;

        static Shopper_Choice()
        {
            MallName = "Shopper's Choice";
            productId++;
        }

        public Shopper_Choice(string pName, int pPrice)
        {
            productName = pName;
            productPrice = pPrice;
            productId++;
        }

        public void SetProductInfo(string proName, double proPrice)
        {
            productName = proName;
            productPrice = proPrice;
           

        }
        public virtual string GetProductInfo()
        {
            return "Product Infomation" + /* "\n" + "Product ID:" +productId +*/ "\n" + "Product Name:"+  productName + "\n" + "Product Price:" + productPrice + "\n";
        }
        public virtual double CalProductPrice(double discount)
        {
           return productPrice -= discount;
        }

        

    }
}
